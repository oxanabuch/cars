package Vehicles;

import Detailsi.Engine;
import Professions.Driver;

public class SportCar extends Car {
    private double speed;

    public SportCar(String brand, String xclass, double weight, Driver driver, Engine engine, double speed) {
        super(brand, xclass, weight, driver, engine);
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "SportCar{" +
                "speed=" + speed +
                "} " + super.toString();
    }
}