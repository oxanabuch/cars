package Vehicles;

import Detailsi.Engine;
import Professions.Driver;

public class Lorry  extends Car {

    private double bodyCapacity;

    public Lorry(String brand, String xclass, double weight, Driver driver, Engine engine, double bodyCapacity) {
        super(brand, xclass, weight, driver, engine);
        this.bodyCapacity = bodyCapacity;
    }

    public double getBodyCapacity() {
        return bodyCapacity;
    }

    public void setBodyCapacity(double bodyCapacity) {
        this.bodyCapacity = bodyCapacity;
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "bodyCapacity=" + bodyCapacity +
                "} " + super.toString();
    }
}