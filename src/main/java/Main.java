import Detailsi.Engine;
import Professions.Driver;
import Vehicles.Car;
import Vehicles.Lorry;
import Vehicles.SportCar;

public class Main {
    public static void main(String[] args) {
        Driver audiDriver = new Driver("Jonson", 15);
        Engine audiEngine = new Engine("500", "Audi");
        Car car = new Car("Audi", "Q", 2800, audiDriver, audiEngine);

        Driver lorryDriver = new Driver("Dizel", 25);
        Engine lorryEngine = new Engine("100", "MAN");
        Lorry lorry = new Lorry("MAN", "S", 8000, lorryDriver, lorryEngine, 10000);

        Driver sportDriver = new Driver("Tomas", 45);
        Engine sportEngine = new Engine("600", "Ferrari");
        SportCar sportCar = new SportCar("Ferrari", "SF", 2500, sportDriver, sportEngine, 350);

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);
    }
}
